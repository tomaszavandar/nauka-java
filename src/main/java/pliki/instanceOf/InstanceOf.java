package pliki.instanceOf;

/**
 * instanceof sprawdza czy cos jest egzemplarzem klasy jakiejs, czy obiekt nalezy do okreslonej klasy
 */
public class InstanceOf {

    public static void main(String[] args) {
        Osoba[] osoba = new Osoba[4];
        osoba[0] = new Pracownik("Janusz", "Kowalski", 1000);
        osoba[1] = new Student("Testowy", "Bonifacy");

        osoba[0].jakisOpis(); // wywola opis z klasy osoba

        for (int i = 0; i < osoba.length; i++) {
            if (osoba[i] instanceof Osoba)  //osoba[i] jest instancja klasy Osoba to wtedy nam wywola opis
                osoba[i].jakisOpis();

        }
    }

}







