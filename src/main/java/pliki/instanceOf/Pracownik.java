package pliki.instanceOf;

public class Pracownik extends Osoba {

    double wynagrodzenie;

    Pracownik(String imie, String nazwisko, double wynagrodzenie) {
        super(imie, nazwisko);
        this.wynagrodzenie = wynagrodzenie;
    }

    @Override
    public void jakisOpis() {
        System.out.println("jestem pracownikiem");
        System.out.println("imie: " + imie);
        System.out.println("nazwisko " + nazwisko);
        System.out.println("Wynagrodzenie " + wynagrodzenie + " zl");
    }
}
