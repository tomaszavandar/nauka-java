package pliki.instanceOf;

public class Student extends Osoba {

    Student(String imie, String nazwisko) {
        super(imie, nazwisko);
    }

    @Override
    public void jakisOpis() {
        System.out.println("jestem studentem");
        System.out.println("imie: " + imie);
        System.out.println("nazwisko" + nazwisko);
    }

}
