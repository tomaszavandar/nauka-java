package pliki.klasaAnonimowa;

/**
 * Klasa anonimowa to taka ktora nie ma nazwy.
 * Klase taka moze uzyc gdy w sytuajcji gdy implementacja czegosc jest dla danego czegos jednorazowa
 * i nie ma tam innej logiki, poza tym jednym zachowaniem, to warto uzyc klasy anonimowej.
 */
public class klasaAnonimowa {

    public static void main(String[] args) {

        akcjaPoWcisnieciu apw = new Przycisk();
        akcjaPoWcisnieciu apw2 = new Przycisk2();

        apw.cos();
        apw2.cos();

        /**
         stworzenie konstruktora klasy anonimowej, ktora da sie wykorzystac gdyz ma od razu
         implementacje metody
         */

        akcjaPoWcisnieciu a = new akcjaPoWcisnieciu() {
            @Override
            public void cos() {
                System.out.println("jestem z klasy anonimowej");

            }
        };

        a.cos();

        Przycisk3 p3 = new Przycisk3();
        p3.dodajAkcje(apw);

    }

}






