package pliki;

public class PorownywanieStringow {
    public static void main(String[] args) {

        String imie = "Janusz";
        String imie2 = "Janusz";

        /*
        Jesli dwa strini sa rowne to zachodzi optymalizacja ze przechowuje je w jednym miejscu.
        Jesli przeslalibysmy do zmiennej imie i imie2 stingi te same to nie beda one rowne gdyz maja one inne adresy
         */

        if (imie == imie2)  // sprawdza to jedynie adresy.
            System.out.println("sa rowne");

        if (imie.equals(imie2)) // porownanie przy pomocy metody "equals" - bezpieczniejsze.
            System.out.println("rowne");


        String kot =  new String("Filemon");
        String kot2 = new String("Filemon");

        if (kot == kot2) // porownuje tu adresy
            System.out.println("sa rowne");
        else
            System.out.println("nie sa");

        if (kot.equals(kot2)) // porownuje tu wartosci stringow
            System.out.println("sa rowne");
        else
            System.out.println("nie sa");
    }
}
