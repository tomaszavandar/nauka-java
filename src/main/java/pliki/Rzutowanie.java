package pliki;

public class Rzutowanie {
    public static void main(String[] args) {

        int a = 8,  b= 6;
        double c = 12, d = 20;

        System.out.println(a / b); // podzieli jedynie liczby calkowite
        System.out.println((double) a / b); // gdy rzutujemy jedna liczbe juz jako double to da poprawny wynik

        int wynik1 = a / (int)d; // mozemy liczbe d = 20 rzutowac do inta (l. calkowitej) i wtedy tracimy wartosc po przecinku i mozna wykonac dzialanie
        System.out.println(wynik1);

        double wynik2 = c / d;
        System.out.println(wynik2);
    }
}
