package pliki.klasaObiekt;

public class klasaObject {

    public static void main(String[] args) {

        Object a = new Punkt(300, 400);

        Punkt p = new Punkt();

        System.out.println("Domyslan wartosc punktu X: " + p.getX());
        System.out.println("Domyslan wartosc punktu Y: " + p.getY());

        Punkt p1 = new Punkt(7, 13);
        System.out.println("Nowe wspolrzedne: " + "X: " + p1.getX() + " Y: " + p1.getY());


        System.out.println(p.getClass());

        System.out.println(p.equals(p1)); // zwroci "false" gdys adresy p i p1 sa rozne.

        System.out.println(p.toString());
        System.out.println(p1.toString());

    }

}


