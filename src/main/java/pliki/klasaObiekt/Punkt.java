package pliki.klasaObiekt;

public class Punkt {


    private int x = 5;
    private int y = 10;

    Punkt() {
        //domyslny konstruktor
    }

    Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (this.getClass() != o.getClass())  //sprawdzneie czy aktualna klasa jest rozna od klasy ktora zostala przeslana
            return false;

        pliki.klasaObiekt.Punkt przeslany = (pliki.klasaObiekt.Punkt) o; // rzutowanie w dol
        return this.x == przeslany.x && this.y == przeslany.y; // zwroci "true" jesli warunek jest spelniony
    }

    @Override
    public String toString() {

        return getX() + " " + getY();

    }
}
