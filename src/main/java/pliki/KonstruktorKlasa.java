package pliki;

public class KonstruktorKlasa {

    int polePierwsze;
    int poleDrugie;

    // Utworzenie konstruktora:

    KonstruktorKlasa(){
        // Utworzony pusty konstruktor
    }

    KonstruktorKlasa(int wartoscPierwsza, int wartoscDruga){
        System.out.println("W konstrukltorze z dwoma wartosciami: ");
        polePierwsze = wartoscPierwsza;
        poleDrugie = wartoscDruga;

    }

    KonstruktorKlasa(int jednaWartosc){
        System.out.println("W konstrukltorze z jedna wartoscią: ");
        polePierwsze = jednaWartosc;
        poleDrugie = 777;
    }

}
