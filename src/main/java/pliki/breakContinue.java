package pliki;

public class breakContinue {
    public static void main(String[] args) {
        /*
        PETLA W PETLI

        Na przykladzie tabliczki mnozenia
        (1 2 3 4 5 6 7 8 9 10)
        x* (...)

        */
        for (int j = 1; j <= 10; j++) {

            for (int i = 1; i <= 10; i++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }

        System.out.println('\n'); // przerwa w linii

        /**
         BREAK I CONTINUE

         continue: przerwanie iteracji od tego momentu i nie wykonuj zadnych instukcji po niej
         break: break przerywa wykonywanie dalej petli i wychodzi z niej. nie wykonuje zadnej juz akcji
         */

        int zmienna;
        for (zmienna = 1; zmienna <= 10; zmienna++) {
            if (zmienna == 5)
                break; // wyjdzie z pętli i nie wypisze nam już od 5 w górę
            System.out.println("Zmienna wynosi: " + zmienna);
        }

        System.out.println('\n');

        int nowaZmienna;
        for (nowaZmienna = 1; nowaZmienna <= 10; nowaZmienna++) {
            if (nowaZmienna == 5)
                continue; // jeśli zmienna wyniesie 5 to ją pominie i będzie dalej wykonywać pętle
            System.out.println("Zmienna wynosi: " + nowaZmienna);
        }

        System.out.println('\n');

        for (int i = 1; i <= 10; i++) {
            if (i == 3)
                continue; // wyprintuje liczy od 1-10 ale bez liczby 3.
            System.out.println(i);

            if (i == 5)
                break; //  wychodzi z petli i  nie wykonuje zadnej juz akcji po
            System.out.println(i);
        }


        // petla nieskończona może być równiez przerwana

        for (zmienna = 1; ; zmienna++) { // gdy nie ma w środku nic to oznacza, ze warunek jest zawsze prawdziwy
            if (zmienna == 5) {
                break;
            }
            System.out.println("Zmienna for: " + zmienna);
        }


    }
}
