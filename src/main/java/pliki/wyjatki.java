package pliki;

public class wyjatki {
    public static void main(String[] args) {


        /***
         Wyjątki typu 'checked' (np. Exception) - musza być obsłuzone
         wyjątki typu 'unchecked' (np. NullPointerException i i nne rozszerzające klase RuntimeException) nie musza być obsługiwane

         Składnia:

         try {
         Instrukcje ktore potencjalnie moga powodowac blad
         }
         catch(NazwaZwroconegoWyjatku nazwaZmiennej) {
         Instrukcje obslugujace zlapany wyjatek
         }
         catch{
         Może być wiele razy "catch" do zlapania wyjatków
         }
         finaly{
         Instrukcje co zawsze zostana wywolane niezaleznie od tego czy powstal wyjatek
         }
         */


        try {
            System.out.println(5 / 0);
            System.out.println("cos"); // instrukcje po tym gdy powstal wyjatek nie zostana wykonane
        } catch (java.lang.ArithmeticException ex) {
            System.out.println("Powstanie wyjatku" + ex.getMessage()); // wyjasnienie typu wyjatku
        } finally {
            System.out.println("cos co zawsze zostanie wywolane");
        }

        System.out.println("cokolwiek");


        /*
        Tworzenie swojego wyjatku
         */

        int a = 5;

        try {
            if (a == 5)
                throw new mojWyjatek("niestety a jest rowne 5");
        } catch (mojWyjatek mw) {
            System.out.println(mw.getMessage());
        }

    }

    static class mojWyjatek extends Exception // nasza klasa "mojWyjatek" rozszerza klase Exception i korzysta z jej metod
    {
        public mojWyjatek(String message) {
            super(message);
        }
    }

}
