package pliki;

public class Zmienne {
    public static void main(String[] args) {

    /*
        Typy calkowite:
        int, long (dla bardzo duzych liczb), byte, short

        Znakowe:
        char - pojedynczy znak, zapisywany w pojedynczych ciapkach
        String - ciag znakow

        pliki.Zmienne zmiennoprzecinkowe:
        float, duble (lepiej uzywac bo dokladniejsze wyniki sa potem)


        NIE WOLNO:
        1) uzywac w nazwie zmiennej keywordow (np public, static)
        2) rozpoczynac nazwy zmiennej od liczby (np. int 54raz)
        3) korzystac w nazwie zmiennej bez spacji


        Dobre praktyki:
        1) uzywanie "underscore" (podkreslnika) tylko dla stalych (np. Liczba_PI = ...)

     */

        // int - integer, l.calkowita
        int a = 10;
        int b = 3;
        int c;
                c = a + b;
        System.out.println(c);

        // String - tekst

        String imie = "Krzysio";
        String nazwisko = "Kowalski";
        System.out.println(imie);

        // char
        char znak = 'A';
        System.out.println(znak);

        // Aby polaczyc String'i uzywa sie normalnie "+" z " " .
        System.out.println(imie + " " + nazwisko);

        // float - w javie automatycznie sie robi double nawet jesli zadeklaruje sie zmienna jako float. Trzeba na koncu dodac "f".

        float liczba = 4.65f; //dodane "f" na koncu, zeby faktycznie uzyl niedokladniejszego float
        System.out.println(liczba);



    }
}
