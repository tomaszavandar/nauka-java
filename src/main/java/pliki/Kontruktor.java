package pliki;
/**
    Konstruktory sa to zbiory instrukcji, ktore maja zostac wykona podczas gdy tworzymy obiekty. Sluzy to skonstruowania obiektu w sposob
    ktory chcemy okreslic. Mozemy okreslac wiele sposobow tylko musza sie roznic parametrami, czyli tymi tymczasowymi zmiennymi ktorymi
    przestaja istniec gdy skonczy sie wywolywanie konstuktora.

    Przy uzyciu slowka kluczowego "this" odwolujemy sie do aktualnego na ktorym operujemy obiekcie, gdy mamy jakies takie same nazwy.

    Konstruktor musi sie identycznie nazywac jak klasa.

    Jeśli nie stworzymy konstruktora to zostanie automatycznie utworzony konstruktor pusty.
 */

public class Kontruktor {
    public static void main(String[] args) {

        // Przykład 1:
        KonstruktorKlasa konstruktorKlasa = new KonstruktorKlasa(3, 18);

//        konstruktorKlasa.polePierwsze  = 22;
//        konstruktorKlasa.poleDrugie = 33;

        System.out.println("Pole pierwsze: " + konstruktorKlasa.polePierwsze);
        System.out.println("Pole drugie: " + konstruktorKlasa.poleDrugie + "\n");

        KonstruktorKlasa konstruktorKlasa1 = new KonstruktorKlasa(55);
        System.out.println("Wartosc z konstruktora: " + konstruktorKlasa1.polePierwsze);
        System.out.println("Wartosc drugia - stała: " + konstruktorKlasa1.poleDrugie + "\n");

        KonstruktorKlasa konstruktorKlasa2 = new KonstruktorKlasa();
        // Wartosci będą zerowe bo takie są domyślne
        System.out.println("Wartość z pola pierwszego z pustego konstruktora: "+ konstruktorKlasa2.polePierwsze);
        System.out.println("Wartość z pola drugiego z pustego konstruktora: " + konstruktorKlasa2.poleDrugie + "\n");


        // Przykład 2:
        Punkt p = new Punkt();
        Punkt p2 = new Punkt(10, 30); //gdy dodamy parametry w konstruktorze to zostanie wywolany drugi

        p.x = 20;
        p.y = 11;

        System.out.println("p.x wynosi: " + p.x);
        System.out.println("p.y wynosi: " + p.y);

        System.out.println("p2.x wynosi: " + p2.x);
        System.out.println("p2.y wynosi: " + p2.y);

    }

    // Mozemy miec wiele konstruktorow ale musza sie roznic parametrami. Parametry to zmienne, ktore moga byc zadeklarowane dla konsruktora tymczasowo
    static class Punkt {
        Punkt() { // konstruktor domyslny powinien przypisywac jakies podstawowe wartosci
            System.out.println("Wywolano konstruktor domyslny");
            x = 20;
            y = 15;
        }
//        Punkt(int pierwszy, int drugi) {
//            System.out.println("Wywolano konstruktor z dwoma parametrami");
//
//            x = pierwszy;
//            y = drugi;
//
//        }

        Punkt(int x, int y) {
            System.out.println("Wywolany trzeci konstruktor");
            this.x = x;
            this.y = y;
        }
        int x;
        int y;
    }
}
