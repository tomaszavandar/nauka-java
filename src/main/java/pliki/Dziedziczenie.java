package pliki;

import dziedziczeniePrzyklad.Potwor;
import dziedziczeniePrzyklad.Szkielet;
import dziedziczeniePrzyklad.Zombie;

public class Dziedziczenie {

    public static void main(String[] args) {

        Potwor p = new Potwor();
        double szybkosc = p.getPredkoscChodzenia();
        System.out.println("Bazowa szybkosc w klasie Potwor to: " + szybkosc);
        double hp = p.getZywotnosc();
        System.out.println("Bazowa zywotnosc w klasie Potwor to: " + hp);

        Szkielet s = new Szkielet(20, 120);
        System.out.println(s.getPredkoscChodzenia());

        Zombie z = new Zombie(7, 200, 300);
        System.out.println(z.getPredkoscChodzenia());
        System.out.println("Mana Zombi wynosi: " + z.getMana());

        s.atak();
    }
}
