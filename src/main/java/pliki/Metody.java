package pliki;

public class Metody {

    public static void main(String[] args) {

        KlasaDlaMetod nazwaObiektu = new KlasaDlaMetod();
        nazwaObiektu.metodaCoNicNieZwraca();
        nazwaObiektu.metodaZArgumentami(3, 'B');

        int jakasZmiennaZWynikiem = nazwaObiektu.wynikZDodawania(10, 3);
        System.out.println("Wynik z dodawania: " + jakasZmiennaZWynikiem);

        double dodawanieLiczbZPrzecinkami = nazwaObiektu.dodawaniePoPrzecinku(23.4);
        System.out.println("Wynik dodawania liczh z przecinkami " + dodawanieLiczbZPrzecinkami);

        boolean zmianaWartosciLogicznej = nazwaObiektu.zmianaWartosciLogicznej(true);
        System.out.println("Zmieniona wartosc logiczna wynosi teraz: " + zmianaWartosciLogicznej);

    }
}



