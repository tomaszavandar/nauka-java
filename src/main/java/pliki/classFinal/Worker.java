package pliki.classFinal;

import java.util.Date;

public class Worker extends Person {
    
    Worker() {
        this.dataZatrudnienia = new Date();
    }

    Worker(Date data) {
        this.dataZatrudnienia = data;
    }

    final Date dataZatrudnienia; //= new Date(); // dla property musimy od razu zadeklarowac wartosc

}
