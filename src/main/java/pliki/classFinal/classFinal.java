package pliki.classFinal;

/**
 * Po ustawieniu klasy typu final, inne podklasy nie moga z niej dziedziczyc.
 * Finalna metoda to taka ktora nie moze byc potem overriden (nadpisana).
 */
public class classFinal {

    public static void main(String[] args) {

        final double PI = 3.14;

        //PI = 9.9; //po wczesniejszym ustawieniu klasy final nie mozna przypisac do niej innej wartosci

        Worker w = new Worker();
        System.out.println(w.dataZatrudnienia);


    }
}



