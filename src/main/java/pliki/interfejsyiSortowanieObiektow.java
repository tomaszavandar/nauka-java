package pliki;


import java.util.Arrays;

/**
 * Interface to teoretycznie klasa ktora bedzie mogla przechowywac metody oraz wlasciwosci tyle, ze wszystkie metody
 * beda od razu i publiczne i abstrakcyjne, a wszystkie wlasciwosci (od razu jak sie je stworzy) publiczne, finalne
 * i statyczne.
 * <p>
 * <p>
 * Interace'y sie implementuje, czyli chcemy nasladowac zachowanie inferface'u. Tworzy sie je po to aby nalozyc pewne
 * zachowania na inne klasy (czyli zimplementowac pewne zachowania)
 * <p>
 * sortowanie obiektow
 */
public class interfejsyiSortowanieObiektow {

    public static void main(String[] args) {

        System.out.println(mojaNazwaInferfejsu.PI);


        int[] tab = new int[3];

        tab[0] = 3;
        tab[1] = -4;
        tab[2] = 1;


        Arrays.sort(tab); // sortowanie tablicy z elementami
        System.out.println(tab[0]); //wypisanie pierwszej wartosci z posortowanych, czyli "-4"

        Pracownik[] pracownik = new Pracownik[3];

        pracownik[0] = new Pracownik(1000);
        pracownik[1] = new Pracownik(200);
        pracownik[2] = new Pracownik(2132);


        for (Pracownik p : pracownik) {
            System.out.println(p.getWynagrodzenie());
        }


        System.out.println("Wynagrodzenie pracownika 2:" + pracownik[2].getWynagrodzenie());
        System.out.println(pracownik[0].compareTo(pracownik[2]));
    }


    interface mojaNazwaInferfejsu {

        double PI = 3.14;   // (wlasciwosc) od razu jest: public, static, final

        void cos(); // metoda jest od razu:  public i abstract

    }

    static class Pracownik implements mojaNazwaInferfejsu, Comparable {

        // klasa Pracownik implementuje inferface, ale musi posiadac rowniez wszystkie publiczne metody jak w inferace'ie

        private double wynagrodzenie;

        Pracownik(double wynagrodzenie) {
            this.wynagrodzenie = wynagrodzenie;

        }

        public double getWynagrodzenie() {
            return this.wynagrodzenie;
        }

        @Override
        public void cos() {

        }

        @Override
        public int compareTo(Object o) {

            Pracownik przeslany = (Pracownik) o;

            if (this.wynagrodzenie < przeslany.wynagrodzenie)
                return -1;
            else if (this.wynagrodzenie > przeslany.wynagrodzenie)
                return 1;

            return 0;

        }
    }


}


