package pliki;

public class klasyZagniezdzone {

    /*
    Statyczna klasa zagniezdzona :

    Zwykla klasa zagniezdzona (bez slowka "static"):


    1.Klasa wewnetrzna ma dostep do pol klasy zewnetrznej.

    2. Klasa zewnetrzna nie ma dostepu do pol klasy wewnetrznej
     */

    public static void main(String[] args) {

        A zewnetrzna = new A(); //wywola sie konstruktor z klasy A

        A.B temp = zewnetrzna.new B(); //wywola sie konstruktor z klasy B.
        /*
        składnia:

        nazwa klasy zewnetrznej.nazwa klasy wewnetrznej zmienna(pod ktora zapiszemy) = zewnetrzna.new B()
         */

        A.C temp2 = new A.C(); //gdy klasa C jest statyczna (czyli istnieje juz) nie musimy sie do od



    }

}


class A
{
    A() //konstruktor
    {
        {
            System.out.println("Konstruktor klasy zewnetrznnej A");
        }
    }
    class B
    {
        B()
        {
            System.out.println("konstruktor klasy zagniezdzonej B");
        }
        void cos ()
        {
            a = 4; //1. Mozemy w klasie B odwolac sie do pola (zmiennej a), ktora jest zdefiniowana w klasie A.
        }

        private int test;
    }

    static class C
    {
        C()
        {
            System.out.println("konstruktor klasy zagniezdzonej C");
        }
    }

    private int a;

    void cos2 ()
    {
        //test = 2; // do zmiennej "test" nalezacej do klasy B, nie mozemy sie odwolac w klasie A.
    }
}