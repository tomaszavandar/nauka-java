package pliki.agregacjaKompozycja;

public class Pracus {

    String imie;
    Adres adres; //agregacja

    //Praca praca; //kompozycja. Kazdy pracownik ma prace

    public Pracus(String imie, Adres adres) {
        this.imie = imie;
        this.adres = adres;
    }

    @Override
    public String toString() {
        return this.imie + ": " + adres.ulica + " " + adres.nrDomu;
    }

}
