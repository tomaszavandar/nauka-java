package pliki;

public class ReferencjaVsZmienna {

    public static void main(String[] args) {

        int a = 5;
        int b = a;

        b = 30;
        System.out.println(a);

        Test x = new Test(); // w X nie znajduje sie zadna wartosc typowa prymitywna a TYLKO ADRES
        Test y = x;

        y.a = 40;

        System.out.println(x.a);
        System.out.println(y.a);

    }

}

class Test
{
    int a = 10;
}

/*
Przekazywanie wartosci referencyjnych i prymitywnych (tj. int, char etc)
 */

