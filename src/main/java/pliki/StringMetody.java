package pliki;

import java.sql.SQLOutput;

public class StringMetody {

    public static void main(String[] args) {

        String przykladowyString = "Jakiś przykłądowy string";

        // Określanie długości stringa:
        System.out.println("Długość stringa wynosi: " + przykladowyString.length());

        // Sprawdzenie czy string jest pusty (zwróci 'true' or 'false'):
        System.out.println("Czy string jest pusty: " + przykladowyString.isEmpty());

        // Wyprintowanie pojedyńczego znaku ze stringa (tutaj printuje osttani znak ze stringa):
        char pojedynczyZnak = przykladowyString.charAt(przykladowyString.length() -1);
        System.out.println("Pojedyńczy znak to: " + pojedynczyZnak);

        // Substring, czyli wycinka ze stringa okreslonych znaków po indeksach
        String wycietyString = przykladowyString.substring(3); // Zaczyna liczyć od indeksu '3' czyli od "i..."
        System.out.println("Wycięty string: " + wycietyString);

        // Substring podając początek i koniec
        String wycietyStringDwa = przykladowyString.substring(4, 12);
        System.out.println("Wycięty string z początkiem i końcem: " + wycietyStringDwa);

        // Trim - czyli usunięcie białych znaków z początka i końca stringa
        String stringZBialymiznakami = " bialy znak przed i po cos jeszcze ";
        System.out.println("String z białymi znakami + kropki, żeby zobaczyć: " +"." + stringZBialymiznakami + " .");
        System.out.println("String po trim: " + "." + stringZBialymiznakami.trim() + ".");

        // LowerCase/UpperCase
        String rozneWielkosciLiter = "Duze litery i MALE HEHEHE";
        System.out.println("To Lower Case: " + rozneWielkosciLiter.toLowerCase());
        System.out.println("To Upper Case: " + rozneWielkosciLiter.toUpperCase());

        // Sprawdzenie czy String zawiera jakis tekst
            // Zwracany jest boolean (wielkość znaków ma również znaczenie): metody .startWith oraz .endsWith
        String czyZawiera = "Czy zawiera jakiś konkretny text";
        System.out.println("Czy zawiera: " + czyZawiera.startsWith("Czy zawie"));
        System.out.println("Czy zawiera koncowce: " + czyZawiera.endsWith("text"));
            // czy zawiera jakiś tekst randomowy
        System.out.println("CZy zawiera jakis randomowy tekst: " + czyZawiera.contains("jakiś"));

        // Replacee czyli zamienie jakiegoś konkretnego znaku na jakiś inny
        System.out.println("Replace: " + czyZawiera.replace("a","W"));


    }

}
