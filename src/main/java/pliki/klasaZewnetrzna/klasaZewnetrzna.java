package pliki.klasaZewnetrzna;

/**
 * Klasa Wewnetrzna jest uzywana gdy dany obiekt klasy będzie użyty tylko raz
 * żeby ne tworzyć odrębnych plików w projekcie (klas), które będa tylko raz użyte
 * <p>
 * Aby użyc w klasie zewnetrznej metod i pól z klasy wewnętrznej trzeba utworzyć najpierw jej obiekt
 */
public class klasaZewnetrzna {

    int poleZewnetrzne;

    public int metodaZewnetrzna() {
        return -1;
    }


    /**
     * W klasie wewnętrznej możemy używać metod i pól z klasy zewnętznej
     */
    class klasaWewnetrzna {

        int poleWewnetrzne;

        public int metodaWewnetzna() {
            return -2;
        }

        public void nowaMetodaWywolujacaMetodeZKlasyZewnetrznej() {
            metodaZewnetrzna(); // wywolanie metody z klasy zewnetrznej

            metodaWewnetzna(); // wywolanie metody z klasy wewnetrznej
        }

    }
}
