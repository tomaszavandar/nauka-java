package pliki;

public class SetteryGetteryModyfikatorPrivate {
    public static void main(String[] args) {

        kontoBankowe oszczednosci = new kontoBankowe();

        //oszczednosci.saldo = 1000; // 1. zmienienie salta z zewnatrz
        //System.out.println(oszczednosci.salto);

        System.out.println(oszczednosci.getSaldo()); //2. pobranie wartosci salda

        int wplacono = oszczednosci.wplac(200);
        System.out.println("Ilosc na koncie po wplaceniu: " + wplacono);

        int wyplacono = oszczednosci.wyplac(150);
        System.out.println("Ilosc kasy na koncie po wyplaceniu: " + wyplacono);


    }


}

class kontoBankowe
{
    public kontoBankowe()
    {
        saldo = 500; //ustawienie poczatkowej wartosci
    }
    private int saldo; //1.  zeby uniknac zmieniania z zewnatrz dodajemy modyfikator private

    int getSaldo()  //2. piszemy GET przed funkcja ktora ma zwrocic jakas wartosc
    {
        return saldo;
    }

    void setSaldo(int saldo) //ustawienie salda przez SET + nazwa funkcji
    {
        /*
           WARUNKI, ktore np beda sprawdzac
         */
        this.saldo = saldo; // odwolanie sie do aktualnego obiektu
    }

    int wyplac(int ileKasyWyplacic)
    {
        if (saldo < ileKasyWyplacic)
            System.out.println("za malo kasy na koncie");
        else
            setSaldo(saldo - ileKasyWyplacic);
        return saldo;
    }

    int wplac(int ileKasyDoWplacenia)
    {
        setSaldo(saldo + ileKasyDoWplacenia);
        return saldo;
    }
}
