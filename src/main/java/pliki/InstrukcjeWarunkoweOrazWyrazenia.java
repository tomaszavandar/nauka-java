package pliki;

public class InstrukcjeWarunkoweOrazWyrazenia {

    public static void main(String[] args) {

        /**
         Instrukcja warunkowa SWITCH:
         Dziala na takiej zasadzie ze przelacza sie do konkretnej instrukcji a nastepnie wykonuje wszystko w dol
         NIE MOZNA porownywac Double i  String'a.
         Mozna porownywac znaki  - char
         */

        int a = 10; // w zaleznosci ile bedzie wynosic zmienna a to wtedy roznie bedzie sie zachowywac instrukcja switch
        char b = 'W';

        switch (a) { //syntax z javy 8
            case 20:
                System.out.println("wynosi cos tam 20");
                break; // gdy dodana jest instrukcja break to wtedy wyprintuje nam case 20 jesli a = 20 i przerwie wykonywanie dalej
            case 40:
                System.out.println("wynosi cos tam 40");
                break;
            default:
                System.out.println("drukowane all");
        }

        switch (b) {
            case 'Z':
                System.out.println("cos 1");
                break;
            case 'W':
                System.out.println("cos 2");
            default:
                System.out.println("cos 3");
        }


        /**
         Wyrazenia Warunkowe:

         Instrukcje warunkowa if np sprawdzajaca czy dana liczba jest parzysta mozna zapisac o wiele szybciej uzywajac wyrazenia warunkowego.
         Skladnia:
         wyrazenie ? tu co ma sie stac jesli wyrazenie to true : co ma sie stac jesli wyrazenie jest false;

         */

        //Uzycie zwyklej instrukcji IF:

        int g = 7;
        int t = 10;

        if (g % 2 == 0) // g modulo 2, czyli jesli g /2 daje reszte z dzielenia 0 to wtedy wiemy ze parzysta
            System.out.println("parzysta");
        else
            System.out.println("nieparzysta");


        // Uzycie wyrazenia warunkowego:

        String czyParzysta = t % 2 == 0 ? "parzysta" : "niepatrzysta";
        System.out.println(czyParzysta);

    }
}
