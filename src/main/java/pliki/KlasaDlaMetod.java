package pliki;

public class KlasaDlaMetod {

    // typZwracany nazwaMetody (parametry) {
    // }

    // metody typu "void" nie zwracają nic

    void metodaCoNicNieZwraca() {
        System.out.println("Wartość wypisana z metody");
    }

    void metodaZArgumentami(int naszaLiczba, char pojedynczyZnak) {
        System.out.println("Wartość z metody: " + naszaLiczba + " " + pojedynczyZnak);
    }

    int wynikZDodawania(int a, int b) {
        int wynik = a + b;
        return wynik;
    }

    double dodawaniePoPrzecinku(double liczba){
        double wynik = liczba + 0.54;
        return wynik;
    }


    boolean zmianaWartosciLogicznej(boolean wartoscLogiczna){
        return !wartoscLogiczna;
    }


}
