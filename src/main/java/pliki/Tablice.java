package pliki;

public class Tablice {

    public static void main(String[] args) {

        /*  TABLICE JEDNOWYMIAROWE:
            Aby stworzyc tablice piszemy typ zmiennej, potem [] i nazwę;
            A potem przypisujemy do tego operatora (tab) za pomoca keyworda "new" ile tych liczb calkowitych chcemy zarezerowwac miejsce w pamieci
            Gdy tablicy nie nadamy wartosci bedzie miala domyslne wartosci. Dla int'a jest to 0.

            -----------------------------------
            tab[0] tab[1] tab[2] tab[3] tab[4]

            -----------------------------------

         */

        int[] tab;
        tab = new int[5]; //zarezerowane miejsce w pamieci dla 5 -ciu miejsc. Mozna od razu wszystko dac w jednej linni wczesniejszej: int[] tab = new int[5]

        tab[0] = 4;
        tab[1] = 5;
        tab[2] = 13;
        tab[3] = 33;
        tab[4] = 9;
        //tab[5] = 21; //to nie zadziala bo wychodzimy poza zakres

         // odwolanie sie do tych miejsc
        System.out.println(tab[3]); //indeksujemy tablice zaczynajac od 0

        tab[2] = 13;
        System.out.println("Zmienna 2 w tablicy wynosi: " + tab[2]);

        // Mozna jeszcze w szybszy sposob od razu zadeklarowac tablice i co bedzie w tej tablicy.

        int[] tab2 = {4, 6, 2, 53, 45, 234};
        System.out.println(tab2[1]);

        // Mozna sprawdzic dlugosc tablicy ile zawiera elementow przy uzyciu "lenght"
        System.out.println("Dlugosc tablicy2 to : " + tab2.length + " elementy");

        // Aby odwolac sie do osttaniego elementu w tablicy musimy przy uzyciu lenght dodac (-1), - bo indeksowanie idzie od 0.
        System.out.println("Ostatni element tab2 wynosi: " + tab2[tab2.length-1]);


        /*
            TABLICE WIELOWYMIAROWE:
            -------------------------------------------------
            tab[0][0] tab[0][1] tab[0][2] tab[0][3] tab[0][4]
            -------------------------------------------------
            tab[1][0] tab[1][1] tab[1][2] tab[1][3] tab[1][4]
            -------------------------------------------------
            tab[2][0] tab[2][1] tab[2][2] tab[2][3] tab[2][4]
            -------------------------------------------------
        */

        int [][] tab3 = new int [3][4];  // 3 - wiersze, 4 - kolumny

        int [][] tab4 =
            {
                {3, 4, 6, 67},
                {12,43,21,-20},
                {22,56,54,32}
            };
        System.out.println(tab4[0][2]);

        // Rozmiar całej tablicy: (są liczone wiersze)
        System.out.println("Rozmiar całej tablicy: " + tab4.length);

        // Rozmiar elementów w danym wierszu:
        System.out.println("Rozmiar elementów w danym wierszu: " + tab4[0].length);


        /*
            TABLICA TRÓJWYMIAROWA

            reszta analogicznie jak dla tablic dwuwymiarowych
         */
        int [][][] tablica = new int[2][3][5];
    }
}
