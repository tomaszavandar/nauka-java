package pliki;

public class PetlaWhile {

    public static void main(String[] args) {

        String[] jakiesRzeczy =
                {
                        "Cos 1",
                        "Cos 2",
                        "Kotek",
                        "Piesek"
                };

        int i = 0;
        while (i < 4)
        {
            System.out.println(i);
            i++;
        }

         //Jesli zmieni sie dlugosc w tablicy "jakiesRzeczy" to lepiej w petli while odwolac sie do dlugosci tablicy

        while (i < jakiesRzeczy.length)
        {
            System.out.println(jakiesRzeczy[i]);
            i++;
        }


        /*
        Petla DO WHILE:
        Uzywa sie jej gdy nie interesuje nas na starcie czy warunek musi byc sprawdzony. W takiej petli najpierw wykonuje akcje a potem sprawdzi na koncu warunek
         */

        int c = 1;

        do {
            System.out.println(jakiesRzeczy[c]);
            c++;
        }
        while (c < jakiesRzeczy.length);



        int liczba = 1;

        while (liczba <=15){
            System.out.println("Generuje nową liczbę: " + liczba);
            liczba++;
        }








    }




}
