package pliki;

public class Operatory {
    public static void main(String[] args) {
    /*

    pliki.Operatory Arytmetyczne:
    +, -, / , *
    += , -=, /=, *=, %=
    % - moduło (reszta z dzielenia)

    ++ Inkrementacja (powiekszenie o 1)
    -- dekrementacja (pomniejszenie o 1)

    y++ POST inkrementacja (wykonuje sie po tym jak wypiszemy wartosc zmiennej na ekranie)
    y-- POST dekrementacja

    ++y PRE inkrementacja
    --Y PRE dekrementacja
    */
        int a = 1 % 3;
        System.out.println(a);

        int b = 4;
        b++; //powiekszenie zmiennej b o 1
        System.out.println(b);

        int c = 6;
        System.out.println(c++); //tu wyprintuje nam jeszcze liczbe 6 i dopiero potem zajdzie jej powiekszenie
        System.out.println(c); // tu wyprintuje wtedy juz ja nam powiekszona


     /*
    pliki.Operatory Relacyjne (porownania):
    ==  przypisania
    != czy jest rozne od czegos
    inne te typu <, >, <=
     */

        boolean isTrue = 4 == 4;
        System.out.println(isTrue);

     /*
    pliki.Operatory Logiczne:
    true, false
    ! - negacja(zaprzeczenie) np !(true) = false
    && - koniunkcja (i)
    || - alternatywa (lub)


     pliki.Operatory Bitowe: (przydaja sie przy kompesji):
    & - iloczyn bitowy
    | - suma bitowa
    ^ - XOR  eXlusive OR
    x << 1 - przesuniecie  w lewo o 1
    x >> 1 - przesuniecie w prawo o 1
    ~ negacja bitowa
     */


    }
}
