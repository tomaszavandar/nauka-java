package pliki;

import java.io.FileReader;
import java.util.*;

public class hashSetTreeSet {

    public static void main(String[] args) {

        Set<String> set = new HashSet<String>(32);
        set = new TreeSet<String>();

        // Wyprintowanie zawartosci pliku jako jednej linii

        try {
            Scanner reader = new Scanner(new FileReader("src\\main\\resources\\jakasNazwa.txt"));

            StringBuilder napis = new StringBuilder();
            while (reader.hasNext()) {
                napis.append(reader.next()).append(" ");
            }
            System.out.print(napis);
            reader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        // Wyprintowanie zawartosci pliku w slowach w oddzielnych liniach (z uzyciem println)

        try {

            Scanner czytacz = new Scanner(new FileReader("src\\main\\resources\\jakasNazwa.txt"));

            while (czytacz.hasNext()) {
                System.out.println(czytacz.next());
            }
            czytacz.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        Iterator<String> iter = set.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

    }


}
