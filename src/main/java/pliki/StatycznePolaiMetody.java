package pliki;

public class StatycznePolaiMetody {

    public static void main(String[] args) {

        //Matematyka m1 = new Matematyka(); // 1. nie musimy tworzyc instancji klasy bo uzywamy metody statycznej w klasie Matematyka

        double wynik = Matematyka.dodaj(10.3, 12.3);
        System.out.println(wynik);

        System.out.println(Matematyka.PI);

    }


    static class Matematyka
    {
        static final double PI = 3.14; // metoda statyczna, niezmienna
        static double dodaj(double a, double b) // 1. gdy mamy metode statyczna mozemy ja wywolac bez tworzenia instancji klasy
        {
            return a + b;
        }
    }
}



