package pliki;

/*
Skladnia petli FOR:
for (Inicjacja zmiennych; Warunek petli; co ma sie dziac po wykonaniu wszysytkich instrukcji w petli)
Warunek jest sprawdzany od razu na starcie
 */
public class petlaFor {
    public static void main(String[] args)
    {
        String [] auta =
        {
                "Auto 1",
                "Auto 2",
                "Auto 3"
        };

        for (int i = 0; i < auta.length; i++)
        {
            System.out.println(auta[i]);
        }

    /*
    Do szybkiego tylko przejscia pomiedzy elementami mozna uzyc petli ENHANCED FOR
     */
        for (String moja_wartosc: auta)
        {
            System.out.println(moja_wartosc);
        }

        new petlaFor().kolejnaPetlaFor();
    }

    public void kolejnaPetlaFor(){

        // for ( ustawienie początkowej wartości; jak długo ma się pętla wykonywać; modyfikacja po każdym wykonaniu pętli(co ma się stać po każdym wykonaniu pętli) )

        int liczenie;
        for (liczenie = 1; liczenie <=10; liczenie= liczenie+1){
            System.out.println("Wynik liczenia: " + liczenie);
        }
        System.out.println("A tu kolejny wynik ile wyniósł gdy wyszło z pętli: " + liczenie);

    }


}
