package pliki.hashCodeFunkcjeMieszane;


/**
 * Hash "#" jest to po prostu liczba calkowita, ktora nazywa sie kodem mieszajacym.
 * # musi byc taki sam dla rownych dla siebie obiektow.
 */
public class hashCodeFunkcjeMieszane {

    public static void main(String[] args) {

        Element a = new Element(3);
        Element b = new Element(3);
        Element c = null;

        System.out.println(a == b); // zwroci falce bo sprawdza po adresach

        System.out.println(a.equals(b));

        System.out.println("Sprawdzenie elemetu a i String'a zwroci: " + a.equals(new String("haha")));

        System.out.println("abc".hashCode()); //metoda dla String'a ma sprawdzenie hashcode'u
        System.out.println("ab".hashCode());

        /**
         Rozne hashcode dla elemetow "a" i "b"
         Gdy mamy nadpisana funkcje odnosnie hashcodow to wtedy gdy nasze elementy beda przyjmowac te same wartosci np 3 i 3
         to hashcode'y beda rowne.
         */
        System.out.println(a.hashCode());
        System.out.println(b.hashCode());

    }
}
