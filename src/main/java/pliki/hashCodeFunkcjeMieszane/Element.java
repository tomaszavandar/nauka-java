package pliki.hashCodeFunkcjeMieszane;

public class Element {

    int wartosc;

    public Element(int wartosc) {
        this.wartosc = wartosc;
    }

    /**
     * zabezpieczenie sprawdzajace czy objekt jest nullem oraz sprawdzenie czy obiekt elementu 'a' jest rozny
     * od porownywanego elementu - w tym przypadku Stringa "haha".
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {

        if (obj == null || this.getClass() != obj.getClass()) return false; //
        return this.wartosc == ((pliki.hashCodeFunkcjeMieszane.Element) obj).wartosc;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 14 * hash + this.wartosc;
        return hash;
    }

}
