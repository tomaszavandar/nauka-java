package pliki;

public class ProgramowanieObiektowe
{
    public static void main(String[] args)
    {
         /*
         Obiekty - to pojemniki do przechowywania zmiennych i funkcji tematycznie ze soba
                   powiazanych do dalszego latwiejszego ponownego uzycia
         Klasy - foremki do tworzenia egzemplarzy obiektow

         Metody - funkcje, okreslajace zachowanie, czynnosci jakie moga byc wykonane przez dana klase
          */

        Monitor abc = new Monitor();
        abc.szerokosc = 123;
        Monitor abc2 = new Monitor();
        abc2.szerokosc = 300;

        System.out.println(abc.szerokosc);
        System.out.println(abc2.szerokosc);


    }

    static class Monitor
    {
        int szerokosc; // wlasciwosci opusujace ten monitor
        int wysokosc;

        void wlacz()
        {

        }
        void wylacz()
        {

        }
    }
}
