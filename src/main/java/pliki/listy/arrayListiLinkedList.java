package pliki.listy;

import java.util.*;

public class arrayListiLinkedList {

    /**
     * ArrayList to tablica, ktora sie dynamiczne rozszerza. Wszystko jest pokolei.
     * LinkedList stosujemy, zeby szybciej odnalezc elementy z listy.
     */
    public static void main(String[] args) {

        List<Double> listaTablicowa = new ArrayList<>();

        Double liczba = 0.1;
        for (int i = 0; i < 5; i++) {
            listaTablicowa.add(liczba + 0.1);
            System.out.println(listaTablicowa.get(i));
        }


        ListaPolaczona listaPolaczona = new ListaPolaczona();
        listaPolaczona.wartosc = 10;

        listaPolaczona.nastepna = new ListaPolaczona();
        listaPolaczona.nastepna.wartosc = 20;

        listaPolaczona.nastepna.nastepna = new ListaPolaczona();
        listaPolaczona.nastepna.nastepna.wartosc = 30;

        System.out.println(listaPolaczona.wartosc);
        System.out.println(listaPolaczona.nastepna.wartosc);
        System.out.println(listaPolaczona.nastepna.nastepna.wartosc);


        LinkedList<String> listaPowiazana = new LinkedList<>();


        listaPowiazana.add("llalala1");
        listaPowiazana.add("llalala2");
        listaPowiazana.add("llalala3");
        listaPowiazana.add("llalala4");


        /**
         *  Jesli bylo by bardzo duzo elementow to przejdzie taka petla po LinkedList trwalo by bardzo dlugo.
         *  W przypadku ArrayList jesli chcemy wyprintowac 200 element to on od razu tam skacze - bo w pamieci
         *  rezerwuje sobie miejsce dla wszystkich elementow na poczatku.
         */

        for (int i = 0; i < listaPowiazana.size(); i++) {
            System.out.println(listaPowiazana.get(i));
        }

        wypiszElementyListy(listaPowiazana);

        Iterator<String> iter = listaPowiazana.iterator();

        iter.hasNext();
//         iter.remove(); //usuwa poprzednika

        System.out.println("Wypisane na koncu");
        wypiszElementyListy(listaPowiazana);

        ListIterator<String> iter2 = listaPowiazana.listIterator(); //uzycie ListIterator powoduje ze mamy dostepnych wiecej metod

        iter2.add("cos1");
        System.out.println(listaPowiazana);

    }

    /**
     * Stworzenie metody iteratora do pobierania
     * Iterator usuwa zawsze poprzednika
     */
    public static void wypiszElementyListy(LinkedList<?> lista) {
        Iterator iteratorListy = lista.iterator();
        while (iteratorListy.hasNext()) //iterator przy uzyciu .hasNext sprawdza czy ma nastepne wywolanie
        {
            System.out.println(iteratorListy.next());
        }

    }
}

