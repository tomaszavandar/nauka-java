package cwiczenia.Klasy.zadanie3_dziedziczenie;

public class Pracownik extends Osoba {

    private String stanowisko;
    private String nazwaFirmy;


    public Pracownik(String imie, String nazwisko, String nazwaFirmy, String stanowisko) {
        super(imie, nazwisko);
        this.stanowisko = stanowisko;
        this.nazwaFirmy = nazwaFirmy;
    }

    @Override
    public void przedstawSie() {
        super.przedstawSie();
        System.out.print(" i pracuje w firmie " + nazwaFirmy + " na stanowisku " + stanowisko);
    }
}
