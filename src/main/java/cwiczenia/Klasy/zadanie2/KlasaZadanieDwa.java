package cwiczenia.Klasy.zadanie2;

public class KlasaZadanieDwa {

    private int[] tablica;

    public KlasaZadanieDwa(int[] podajTablice) {
        tablica = podajTablice;
    }


    public Integer sumaElementow() {
        int suma = 0;

        for (int i = 0; i < tablica.length; i++) {
            suma += tablica[i];
        }
        return suma;
    }

    public Integer srednia() {

        return sumaElementow() / tablica.length;
    }

    public Integer min() {
        int min = Integer.MIN_VALUE;


        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] < min) {
                min = tablica[i];
            }

        }
        return min;
    }

    public Integer max() {
        Integer max = Integer.MIN_VALUE;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
        }
        return max;
    }
}
