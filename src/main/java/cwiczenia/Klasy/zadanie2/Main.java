package cwiczenia.Klasy.zadanie2;

public class Main {

    public static void main(String[] args) {

        int[] tablica = {2, 213, 4, 54, 23, 33};

        KlasaZadanieDwa klasaZadanieDwa = new KlasaZadanieDwa(tablica);


        System.out.println("Suma: " + klasaZadanieDwa.sumaElementow());
        System.out.println("Średnia: " + klasaZadanieDwa.srednia());
        System.out.println("Min value: " + klasaZadanieDwa.min());
        System.out.println("Max value: " + klasaZadanieDwa.max());


    }
}
