package cwiczenia.Klasy.zadanie1;

public class Gamer {

    private String imie;
    private String nazwisko;
    private String jezyk;
    private double zarobki;


    public Gamer(String podajImie, String podajNazwisko, String podajJezyk, double podajZarobki){
        this.imie = podajImie;
        this.nazwisko = podajNazwisko;
        this.jezyk = podajJezyk;
        this.zarobki = podajZarobki;
    }

    public String pobierzImie(){
        return imie;
    }

    public String pobierzNazwisko(){
        return nazwisko;
    }

    public String pobierzJezyk(){
        return jezyk;
    }

    public double pobierzZarobki(){
        return zarobki;
    }


}
