package cwiczenia.petla;


public class zadania {

    public static void main(String[] args) {

        /**
         * 1. Wypisz na ekran liczby od 0 do 30, a następnie od 30 do 0.
         */
        System.out.println("Zadanie 1:");
        for(int i = 0; i<31; i++){
            System.out.print(i + " ");
        }
        System.out.println();
        for (int j = 30; j >=0; j--){
            System.out.print(j+ " ");
        }

        System.out.println("\n");

        /**
         * 2. Napisz program, które wypisze tylko liczby parzyste z zakresu od 0 do 30.
         */
        System.out.println("Zadanie 2:");
        for (int i = 0; i<=30; i++){
            if (i%2 ==0){
                System.out.print(i+ " ");
            }
        }
        System.out.println("\n");
        /**
         * 3. Podaj dwie dowolne liczby a i b, gdzie a jest mniejsze od b (a < b). Wypisz na ekran zakres liczb od a do b.
         */
        System.out.println("Zadanie 3:");
        int a = 5;
        int b = 10;

        for (int i = a; a<=b; a++){
            System.out.print(a + " ");
        }
        System.out.println("\n");

        /**
         * 4. Utwórz tablicę rozmiaru 10 elementów i uzupełnij liczbami. Wypisz wszystkie liczby z tej tablicy na ekran. Następnie wypisz wszystkie liczby od tyłu.
         */
        System.out.println("Zadanie 4:");
        int [] tab = {12, 23, 43, 43, 2, 6, 4, 6, 55, 8};

        for(int i = 0; i<10; i++){
            System.out.print(tab[i] + " ");
        }
        System.out.println("\n");
        for (int i = 9; i>=0; i--){
            System.out.print(tab[i]+ " ");
        }
        System.out.println("\n");

        /**
         * 5. Wykorzystując tablicę z poprzedniego zadania, oblicz sumę wszystkich jej elementów.
         */
        System.out.println("Zadanie 5:");
        int wynik = 0;
        for (int i = 0; i<tab.length; i++){
            wynik += tab[i];
        }
        System.out.println("Suma elementow tablicy wynosi: " + wynik );
        System.out.println("\n");

        /**
         * 6. Napisz program, który będzie obliczał silnię z nieujemnej liczby tj. jeżeli podamy liczbę 5, to zostanie obliczona 5! (wykrzyknik to znak silni).
         * Obliczamy to w następujący sposób:
         * 5! = 5 * 4 * 3 * 2 * 1 = 120.
         * Uważaj, bo silnia kolejnych liczb bardzo szybko rośnie i łatwo wyjść poza rozmiar typu int.
         */
        System.out.println("Zadanie 6:");
        int liczba = 3;
        long wynikZSilni = 1;

        for (int i = liczba; i>=1; i--){
           wynikZSilni = wynikZSilni* i;
        }
        System.out.println("Wynik z silni wynosi: " + wynikZSilni);
        System.out.println("\n");

        /**
         * 7. Napisz program, w którym po podaniu liczby n narysujesz za pomocą * (gwiazdki) trójkąt prostokątny o przyprostokątnych składających się z n-razy * (gwiazdki) ;)
         * np. podając n = 5 powinniśmy otrzymać taki trójkąt:
         *                  *
         *                  **
         *                  ***
         *                  ****
         *                  *****
         */
        System.out.println("Zadanie 7:");
        int podanaLiczba = 3;
        int liczbaGwiazdek = 1;

        for (int i = 1; i<=podanaLiczba ; i++){
            for (int j=1; j<=liczbaGwiazdek; j++){
                System.out.print("*");
            }
            System.out.println();
            liczbaGwiazdek++;
        }





















    }

}
