package cwiczenia.string;

import jdk.nashorn.internal.runtime.regexp.joni.ast.StringNode;

import java.awt.*;
import java.lang.reflect.Array;

public class zadania {

    public static void main(String[] args) {


        /**
         * 1. Utwórz trzy zmienne typu String. Do pierwszej przypisz swoje imię, do drugiej warzywo oraz do trzeciej imię i nazwisko oddzielone spacją.
         * Następnie wykonaj polecenia używając odpowiednich metod z String (nie robić nic ręcznie! :) ) i wypisując informacje na ekran:
         *
         * a) wypisz ile znaków zawiera zmienna przechowująca imię, ile nazwisko a ile imię i nazwisko
         *
         * b) sprawdź czy Twoje imię jest równe "Alicja" lub "Jan"
         *
         * c) wypisz nazwisko z WIELKICH LITER
         *
         * d) zamień w nazwisku litery 'a' na 'e' i zobacz czy coś się zmieniło
         */

        // Ad. 1
        System.out.println("Zadanie 1: ");
        String zmienna1 = "Tomasz";
        String zmienna2 = "Marchew";
        String zmienna3 = "Tomasz Marchew";
        System.out.print("\n");

            // a)
        System.out.println("Zmienna1 zawiera: " + zmienna1.length() + " znaków");
        System.out.println("Zmienna2 zawiera: " + zmienna2.length() + " znaków");
        System.out.println("Zmienna3 zawiera: " + zmienna3.length() + " znaków" + "\n");

            // b)
        String zmiennaAlicja = "Alicja";
        String zmiennaJan = "Jan";

        if (zmienna1.equals(zmiennaAlicja)){
            System.out.println("Zmienna1 " +  zmienna1 + " jest równa " + zmiennaAlicja + "\n");
        }
        else {
            System.out.println("Zmienna1 " + zmienna1 + " nie jest równa " + zmiennaAlicja + "\n");
        }
            // c)
        System.out.println("Nazwisko z wielkich liter: " + zmienna2.toUpperCase() + "\n");

            // d)
        System.out.println("Zamiana liter w nazwisku: " + zmienna2.replace("a","e"));


        /**
         * Dla zdania "Potrafię coraz więcej." wypisz każdy znak w osobnej linii
         */
        System.out.println("Zadanie 2: ");
        String text = "Potrafię coraz więcej.";

        for (int i=0; i <text.length() ; i++){
            System.out.println(text.charAt(i));
        }


        /**
         * 3. Tym razem powyższe zdanie „Potrafię coraz więcej.” napisz od tyłu
         */
        System.out.println("Zadanie 3: ");

        for (int i = text.length()-1; i>=0; i--){
            System.out.println(text.charAt(i));
        }


    }
}
