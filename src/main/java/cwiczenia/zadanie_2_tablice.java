package cwiczenia;

import java.util.Random;

public class zadanie_2_tablice {

/*
    1. Uzupełnij tablicę typu int rozmiaru 5 dowolnymi wartościami. Następnie zsumuj wszystkie elementy tej tablicy i wynik wypisz na ekran.
        Sumowania dokonaj poprzez dodawanie elementów odwołując się do indeksów tablicy.

    2. Utwórz tablicę dwuwymiarową typu double wymiaru 3x5 i także wypełnij dowolnymi wartościami.
    Tym razem zsumuj elementy z każdego wiersza osobno i wypisz wynik na ekran.
 */

    public static void main(String[] args) {

        int [] tablica1 = new int[5];

        tablica1[0] = 2;
        tablica1[1] = 7;
        tablica1[2] = 12;
        tablica1[3] = 3;
        tablica1[4] = 4;

        // Szybszy sposób na uzupełnienie wszystkich wartości w tablicy przy użyciu pętli for
        Random random = new Random();
        for (int i = 0; i<tablica1.length; i++){
            tablica1[i] = random.nextInt(10);
            System.out.println(tablica1[i]);
        }


        int sumaElementówTablicy = tablica1[0] + tablica1[1] + tablica1[2] + tablica1[3] + tablica1[4];
                //tablica1.length-1;
        System.out.println("Suma elementów tablicy: " + sumaElementówTablicy);

       



    }
}