package cwiczenia.instrukcjeWarunkowe;

import java.util.Scanner;

public class zadanie2 {

    /**
     * Obliczniee podatku od dochodu.
     * Wzór:
     *       dla dochodu do 85 528 zł podatek wynosi 17%,
     *      dla dochodu powyżej 85 528 zł liczy się go następująco:
     * - obliczamy podatek 17% z 85 528,
     * - dodatkowo obliczamy podatek 32% od nadwyżki ponad 85 528 zł,
     * czyli dochód pomniejszony o 85 528 zł i z tego 32%.

     */

    public static void main(String[] args) {

        final int progDochodu = 85528;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Obliczanie podatku od dochodu." + "\n" + "Podaj swój dochód roczny: ");
        double dochod = scanner.nextDouble();

        if (dochod > 0 && dochod <= 85528){
            double dochod1 = (dochod * 0.17);
            System.out.println("Podatek od Twojego dochodu wynosi: " + dochod1);
        } else if (dochod == 0) {
            System.out.println("Nie zapłacisz podatku bo masz zerowy dochód");
        } else if (dochod < 0) {
            System.out.println("Podany dochód nie może być ujemny!");
        } else if (dochod > progDochodu){
            double podatekOdDochodu = (progDochodu * 0.17);

            double nadwyzka = (dochod - progDochodu);
            double nadwyzkaOdProguDochodu = (0.32 * nadwyzka);

            double podatek =  (podatekOdDochodu + nadwyzkaOdProguDochodu);
            System.out.println("Twoj podatek wynosi: " + podatek);
        }


    }
}
