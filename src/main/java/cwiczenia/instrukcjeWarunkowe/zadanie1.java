package cwiczenia.instrukcjeWarunkowe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class zadanie1 {

    /**
     * Zadanie 1:
     * Sprawdź czy wpisana liczba jest parzysta czy nieparzysta. Wypisz odpowiedni komunikat na ekran.
     * (podpowiedź: jeżeli reszta z dzielenia przez 2 jest równa 0 to znaczy, że liczba jest parzysta).
     */

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        try {
            int liczba;
            liczba = scanner.nextInt();
            System.out.println("Podana przez ciebie liczba to: " + liczba);

            if (liczba % 2 == 0) {
                System.out.println("Podana liczba jest parzysta");
            } else {
                System.out.println("Podana liczba jest nieparzysta");
            }

        } catch (InputMismatchException e) {
            System.out.println("Podana liczba musi być liczbą całkowitą!");
        }

    }
}
