package dziedziczeniePrzyklad;

public class Zombie extends Potwor
{
    int mana = 120;

    public Zombie()
    {
        System.out.println("Domyslny konstruktor klasy Zombie");
    }

    public int getMana()
    {
        return mana;
    }

    public Zombie(double predkoscChodzenia, double zywotnosc, int mana)
    {
        super(predkoscChodzenia, zywotnosc);
        this.mana = mana;
        System.out.println("Niedomyslny konstruktor klasy Zombie");
    }


}
