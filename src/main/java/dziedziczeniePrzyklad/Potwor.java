package dziedziczeniePrzyklad;

public class Potwor {
    private double predkoscChodzenia = 10;
    private double zywotnosc = 100;

    public void atak() {
        /*
        tu by bylo jakies wykonywane bazowe rzeczy
         */
        System.out.println("To jest metoda atak z klasy Potwor");
    }

    public double getPredkoscChodzenia() {
        return predkoscChodzenia;

    }

    public double getZywotnosc() {
        return zywotnosc;
    }


    public Potwor() {
        System.out.println("Domyslny konstruktor z klasy Potwor");
    }

    public Potwor(double predkoscChodzenia, double zywotnosc) {
        this.predkoscChodzenia = predkoscChodzenia;
        this.zywotnosc = zywotnosc;
        System.out.println("Niedomyslny konstruktor z klasy Potwor");
    }
}
