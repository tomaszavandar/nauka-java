package dziedziczeniePrzyklad;

public class Szkielet extends Potwor
{

    public Szkielet()
    {
        System.out.println("Domyslny konstruktor klasy Szkielet");
    }

    public Szkielet(double predkoscChodzenia, double zywotnosc)
    {
        super(predkoscChodzenia, zywotnosc);
        System.out.println("Niedomyslny konstruktor klasy Szkielet");
    }

    @Override //adnotacja odnosnie nadpisania metody
    public void atak()
    {
        super.atak(); // wywolanie na poczatku metody z klasy Potwor a dopiero reszty z klast Szkielet
        /*
        dodatkowe rzeczy jakieś co ma tylko szkielet
         */
        System.out.println("To jest metoda atak z klasy Szkielet");
    }


}
